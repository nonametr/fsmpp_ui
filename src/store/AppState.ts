import { VuexModule, Module, Mutation, Action } from 'vuex-module-decorators'

const electron = window.require("electron");

const regex = new RegExp("FSM::State\\s*(.+)\\s*\\(\\s*Context\\s*\\*\\s*.+\\)");

@Module({ namespaced: true, name: 'AppState' })
class AppState extends VuexModule {
    public side_pannel = false
    public selected_node_name = "Unknown"
    public current_fsm_path = ""
    public all_FSMs: Array<string> = []
    public cpp_code = "";
    public mermaid_code = "";

    @Mutation
    public setSidePannel(new_val: boolean): void {
        this.side_pannel = new_val
    }

    @Mutation
    public async setCurrentFSM(new_val: string): Promise<void> {
        const fsm_found = this.all_FSMs.find(fsm => fsm === new_val)
        if (fsm_found === undefined) {
            console.log("Bad FSM in `setCurrentFSM`");
            return;
        }

        this.cpp_code = ""
        this.selected_node_name = ""
        this.current_fsm_path = new_val
    }

    @Mutation
    public setSelectedNode(new_val: string): void {
        this.selected_node_name = new_val
    }

    @Mutation
    public setMermaidCode(new_val: string): void {
        this.mermaid_code = new_val;
    }

    @Mutation
    public setCppCode(new_val: string): void {
        this.cpp_code = new_val
    }

    @Mutation
    public setFSMList(new_val: Array<string>): void {
        this.all_FSMs = new_val
    }
}
export default AppState