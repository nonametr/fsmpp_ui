import Konva from "konva";
import Vec2D from "./helpers/vector"

export class Node {
    constructor(shape: Konva.Shape, label: Konva.Text) {
        this.shape = shape;
        this.label = label;
    }

    public shape!: Konva.Shape;
    public label!: Konva.Text;
    public connections: Array<Connection> = [];

    public setPos(x: number, y: number) {
        this.shape.x(x);
        this.shape.y(y);
        this.label.x(x + 0.5 * Node.default_radius);
        this.label.y(y - 2 * Node.default_radius);
    }

    public static readonly default_radius = 20;
}

export class Connection {
    constructor(arrow: Konva.Arrow, from: Node, to: Node) {
        this.arrow = arrow;
        this.from = from;
        this.to = to;
    }
    public arrow!: Konva.Arrow;
    public from!: Node;
    public to!: Node;

    public updateConnection() {
        const vec = new Vec2D(
            this.from.shape.attrs.x - this.to.shape.attrs.x,
            this.from.shape.attrs.y - this.to.shape.attrs.y
        );
        vec.normalize();
        const vx1 = this.from.shape.attrs.x - vec.x * 20;
        const vy1 = this.from.shape.attrs.y - vec.y * 20;

        const vx2 = this.to.shape.attrs.x + vec.x * 20;
        const vy2 = this.to.shape.attrs.y + vec.y * 20;

        this.arrow.attrs.points = [vx1, vy1, vx2, vy2];
    }
}