
export default class Vec2D {
    public x: number;
    public y: number;

    constructor(x: number, y: number) {
        this.x = x
        this.y = y
    }

    normalize() {
        const length = Math.sqrt(this.x * this.x + this.y * this.y); //calculating length
        this.x = this.x / length; //assigning new value to x (dividing x by length of the vector)
        this.y = this.y / length; //assigning new value to y
    }
}