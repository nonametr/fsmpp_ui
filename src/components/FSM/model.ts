export class State {
    constructor(name: string, code: string, line: number) {
        this.name = name;
        this.code = code;
        this.line = line;
    }
    public name!: string;
    public transitions: Array<State> = [];
    public code!: string;
    public line!: number;
}

export class FSM {

    public states: Array<State> = [];
    private state_regex = /\s{0,3}FSM::State\s*(.+)\s*\(\s*Context\s*\*\s*.+\)/g;
    private transition_regex = /FSM::State.+&(.*[^\\)])\s*\)*;|return\s*(nullptr)\s*;/g;

    constructor() {
        //this.states = [new State("END", "", 0)];
    }

    public findState(name: string): State | undefined {
        return this.states.find(state => state.name === name);

    }

    public async load(data: string) {
        const states_found = [...data.matchAll(this.state_regex)]

        await states_found.forEach(async el => {
            const full_match = el[0];
            const state_name = el[1];

            const state_name_start = data.indexOf(full_match);
            const state_body_start = data.indexOf("{", state_name_start);

            let char_id = state_body_start;
            let brackets_cntr = 1;
            while (brackets_cntr > 0 && ++char_id < data.length) {
                if (data[char_id] === "{") {

                    brackets_cntr++;
                }
                if (data[char_id] === "}") {
                    brackets_cntr--;
                }
            }

            const code = data.substring(state_name_start, char_id + 1)

            const new_state = new State(state_name, code, data.substring(0, state_name_start).split("\n").length);

            this.states.push(new_state)
        })

        await this.states.forEach(state => {
            const transitions_found = [...state.code.matchAll(this.transition_regex)];
            transitions_found.forEach(el => {
                if (el[1] !== undefined) {
                    const trans_state = this.findState(el[1]);
                    if (trans_state === undefined) {
                        console.log("Error! State not found!");
                        return;
                    }
                    state.transitions.push(trans_state)
                }
                if (el[2] !== undefined) {
                    //state.transitions.push(this.findState("END")!);
                }
            })
        })
    }

    public serialize() {
        const test = `void run(TContext& context) {
            State* state = new State::Start();
            
            while(state->valid())
            {
                state = state->execute(context);
            }
        }`;

        let result = "#include <map>\n\nnamespace Gen1FSM\n{\n\tnamespace States\n\t{"
        this.states.forEach(state => {
            result += `
        struct ${state.name} : public State
        {
            virtual void run() override;
            
            void transition(std::string new_state)
            {
                ASSERT_WITH_CODE(transitions.find(new_state) != transitions.end(), return; "Error! Incorrect state transition!");

                //do state change
            }

            const std::set<std::string> transitions = {${state.transitions.filter(trans_state => trans_state.name !== state.name).map(state => "\"" + state.name + "\"")}};
            TContext context;
        };
`
        })
        result += "\t}//namespace States\n}//namespace Gen1FSM"
        return result;
    }
}