"use strict";

import electron from "electron"
import { app, protocol, BrowserWindow } from "electron";
import { createProtocol } from "vue-cli-plugin-electron-builder/lib";
import installExtension, { VUEJS_DEVTOOLS } from "electron-devtools-installer";
import fs from "fs"
import { exec } from "child_process";

import "./server/index"

const fs_path = require("path")

let win!: BrowserWindow;

const isDevelopment = process.env.NODE_ENV !== "production";

electron.ipcMain.handle('clion_open', async (event, { file, line }) => {
  return exec("/home/nonametr/clion-2020.3/bin/clion.sh --line " + line + " " + file, (error, stdout, stderr) => {

  });
})

function onFileChange(eventType: string, filename: string, dir: string) {
  win.webContents.send("fsm_update", filename, dir);
}

async function getFolderFSMs(dir_path: string) {
  const entries = await fs.promises.readdir(dir_path, { withFileTypes: true });
  let result = entries.filter(entry => entry.isFile() && entry.name.includes("_fsm.cpp")).map(el => `${dir_path}${el.name}`);
  let dirs = entries.filter(entry => entry.isDirectory());

  for (let dir of dirs) {
    result.push(...await getFolderFSMs(`${dir_path}${dir.name}/`));
  }

  return result;
}

electron.ipcMain.handle('read_file', async (event, path) => {
  return await fs.promises.readFile(path, 'utf8')
})

electron.ipcMain.handle('list_fsms', async (event, { path }) => {
  const all_fsms = await getFolderFSMs(path);

  const all_dirs = all_fsms.reduce((acc: Set<string>, curr) => {
    acc.add(fs_path.dirname(curr))
    return acc
  }, new Set([]))

  await all_dirs.forEach(dir => {
    fs.watch(dir, (eventType: string, filename: string) => {
      onFileChange(eventType, filename, dir)
    });
  })

  return all_fsms;
})

// Scheme must be registered before the app is ready
protocol.registerSchemesAsPrivileged([
  { scheme: "app", privileges: { secure: true, standard: true } },
]);

async function createWindow() {
  // Create the browser window.
  win = new BrowserWindow({
    width: 1200,
    height: 1000,
    webPreferences: {
      nodeIntegration: true,
      // Use pluginOptions.nodeIntegration, leave this alone
      // See nklayman.github.io/vue-cli-plugin-electron-builder/guide/security.html#node-integration for more info
      //nodeIntegration: (process.env
      //  .ELECTRON_NODE_INTEGRATION as unknown) as boolean,
    },
  });

  if (process.env.WEBPACK_DEV_SERVER_URL) {
    // Load the url of the dev server if in development mode
    await win.loadURL(process.env.WEBPACK_DEV_SERVER_URL as string);
    if (!process.env.IS_TEST) win.webContents.openDevTools();
  } else {
    createProtocol("app");
    // Load the index.html when not in development
    win.loadURL("app://./index.html");
  }
}

// Quit when all windows are closed.
app.on("window-all-closed", () => {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== "darwin") {
    app.quit();
  }
});

app.on("activate", () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (BrowserWindow.getAllWindows().length === 0) createWindow();
});

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on("ready", async () => {
  if (isDevelopment && !process.env.IS_TEST) {
    // Install Vue Devtools
    try {
      await installExtension(VUEJS_DEVTOOLS);
    } catch (e) {
      console.error("Vue Devtools failed to install:", e.toString());
    }
  }
  createWindow();
});

// Exit cleanly on request from parent process in development mode.
if (isDevelopment) {
  if (process.platform === "win32") {
    process.on("message", (data) => {
      if (data === "graceful-exit") {
        app.quit();
      }
    });
  } else {
    process.on("SIGTERM", () => {
      app.quit();
    });
  }
}
