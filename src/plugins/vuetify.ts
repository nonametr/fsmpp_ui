import Vue from "vue";
//import Vuetify from "vuetify/lib/framework";
import Vuetify from 'vuetify';

import { library, config } from '@fortawesome/fontawesome-svg-core'
import { faPhone, faEllipsisV, faEnvelope, faSortAlphaDown, faSortAlphaUp, faStreetView, faTrophy, faMapMarkedAlt, faListUl, faThumbsUp } from '@fortawesome/free-solid-svg-icons'
import { faInternetExplorer } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(faPhone, faEllipsisV)

config.autoAddCss = false

Vue.component('fa-icon', FontAwesomeIcon)


Vue.use(Vuetify);

export default new Vuetify({

});
