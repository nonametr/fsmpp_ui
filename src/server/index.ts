import express from "express";
import path from "path";
import fs from "fs"

const app = express();
const port = 8888; // default port to listen

app.get("/", (req, res) => {
    res.send("bad url");
});

interface ReadFileReq {
    query: {
        path: string;
    };
}

interface ListFilesReq {
    query: {
        root: string;
        patern: string;
    }
}

async function getFolderFSMs(dir_path: string) {
    const entries = await fs.promises.readdir(dir_path, { withFileTypes: true });
    let result = entries.filter(entry => entry.isFile() && entry.name.includes("_fsm.cpp")).map(el => `${dir_path}${el.name}`);
    let dirs = entries.filter(entry => entry.isDirectory());

    for (let dir of dirs) {
        result.push(...await getFolderFSMs(`${dir_path}${dir.name}/`));
    }

    return result;
}

app.get("/read_file", async (req: ReadFileReq, res) => {
    try {
        const data = fs.readFileSync(req.query.path, 'utf8')
        res.json({ data })
    }
    catch (error) {
        res.json(error)
    }
});

// start the express server
app.listen(port, () => {
    // tslint:disable-next-line:no-console
    console.log(`server started at http://localhost:${port}`);
});