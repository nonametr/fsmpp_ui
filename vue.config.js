module.exports = {
  transpileDependencies: ["vuetify"],
  pluginOptions: {
    electronBuilder: {
      mainProcessWatch: ['src/server/index.ts'],
    }
  }
};
